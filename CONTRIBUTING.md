# Git workflow

Commit messages **MUST** follow the [Gitmoji](https://gitmoji.dev) convention.

New changes **SHOULD** be done in a separated branch with a dedicated Merge
Request.

# Merge Requests

Merge Requests **MUST** be reviewed before being merged.

Merge requests **SHOULD** rebase their branch on `main` before merging to avoid
merge conflicts and keep a linear Git history.
