How To: Use CMDB Operations Job Template
===========================================================
**AWX Data Preparation:**
        Clone the repository:
        ``git clone https://code.europa.eu/digit-c4/awx-data.git``

        Add your AWX instance details:

**1.** Create new branch from ``main``

**2.** Edit ``ansible/hosts`` file and add your AWX details:
    
        | [service_yourServiceName]
        | yourAwxHostWithoutHTTPprotocolAndPort
        For example:

        .. code-block:: yaml
           :linenos:
        
                [cmdb_workplace]
                vawx-131.lab.snmc.cec.eu.int
        
**3.** Create ``service_yourServiceName`` folder inside ``awx-data/ansible/group_vars`` 

**3.1** Create ``cmdb.yml`` file there - name corespond to the role you are going to execute. 
                See example: ``awx-data/ansible/group_vars/cmdb_workplace/cmdb.yml``
        
**4.** Insert the variables into your file (example):
                Consult the :doc:`this page <new-bubble>`, CMDB section for information.
                
                *Make sure the awx_host variable is set, if not, set it in cmdb.yml*
                
**Jobs preparation:**

**1.** **Dump and Store Job template**
        This job is responsible for backing a netbox database and storing the backup in the vlog server.
        In order for this job to run successfully theres 2 records that need to be added to the inventory used by the job:

            - ``Netbox Host:`` Used in the job `"[CMDB] Dump Database"`

            - ``Vlog Host:`` Used in the job `"[CMDB] Retrieve backup and store on SFTP"`

        This workflow, `"Netbox-lab backup workflow"`, will be executed daily at 18:00 PM CET by the scheduler `"Netbox-lab backup workflow"`

**2.** **Password Rotation**
        This job is responsible for the password rotation used by the user to login into netbox. 
        Job executed by the scheduler `"Netbox-lab Sunday Password Rotation Schedule"` that runs weekly every sunday at 3 AM CET
        The new password is stored in the Vault.

        **Vault Preparation**

        | Contact CMDB Squad memeber to create vault placeholder for your Netbox instance. Directly on Teams or create card on [CMDB board](https://globalntt.leankit.com/board/31512194691730).
