How To: Add a new environment
=============================

A bubble can have as many environments as you want. They are used to group
RPS clusters and mappings, and usually are served through a dedicated DNS zone.

This guide will assume the bubble has 3 environments:

 - the ``NMS`` environment for management mappings (Netbox, AWX, ...)
 - a ``Production`` environment in ``intracloud`` (specific to AWS bubbles)
 - a ``Production`` environment in ``webcloud`` (specific to AWS bubbles)

**1)** Edit the file `ansible/group_vars/my_new_bubble/rps.yml` (adjust to your requirements):

.. code-block:: yaml
   :linenos:

   rps_internal_environments:
     - name: nms
       via: tech
       dns_zone: mybubble.nms.tech.ec.europa.eu
       waf_labels: "{{waf_labels_acme_nms }}"
     - name: production
       via: intracloud
       dns_zone: mybubble.intracloud.ec.europa.eu

   rps_external_environments:
     - name: production
       via: webcloud
       dns_zone: mybubble.webcloud.ec.europa.eu

**2)** Adjust your ``rps_playbooks`` variable as you need.

**3)** Consult :doc:`this page <cicd>` to run the pipeline and provision the AWX.

**4)** Consult :doc:`this page <install-rps>` to install the RPS in the bubble.
