How To: Provision an AWX
========================

**1)** Click on this button:

.. image:: https://img.shields.io/badge/ci-RUN%20PIPELINE-blue?style=flat-square
   :target: https://code.europa.eu/digit-c4/awx-data/-/pipelines/new
   :alt: RUN PIPELINE

**2)** Select the branch from which to deploy (usually ``main``).

**3)** Specify the variable ``MANUAL_TARGET`` with the name of your bubble,
or ``all`` to provision all bubbles all at once.

**4)** Specify the variable ``TAG_NAME`` with the value ``all`` to run every task tagged or untagged or use the value ``cmdb`` to deploy just the task related to cmdb.

**5)** Choose the correct Gitlab CI runner for your bubble's location:

.. csv-table::
   :header: "``RUNNER_NAME``", Location
   :widths: 20, 80

   "``lab``", "For bubbles in the datacenter or in OVHCloud"
   "``rpscurua``", "For bubbles in AWS"
   "``bimbimbapdata``", "For bubbles in the datacenter, but not reachable from the lab"

**6)** Specify the variable ``FORCE_SYNC``, if set to ``true`` will force AWX to
resync the inventories. `(this step is optional)`

**7)** Click on the ``Run Pipeline`` button.
