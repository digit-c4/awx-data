How To: Add a new bubble
========================

**1)** Add a hostgroup to the ``ansible/hosts`` file:

.. code-block:: ini

   [bubble_mynewbubble]
   awx.host.tld

   [group_bubbles_aws:children]
   bubble_mynewbubble

   ## OR:
   #
   # [group_bubbles_dc:children]
   # bubble_mynewbubble

If the AWX is not yet exposed (via a Reverse Proxy for example), you will need
to use its IP address.

**2)** Add a folder ``ansible/group_vars/bubble_mynewbubble/`` to store the bubble's
specific configuration.

You will need to create the following files:

``ansible/group_vars/bubble_mynewbubble/awx.yml``:

.. code-block:: yaml
   :linenos:

   ---

   # or http://<internal elb>:8013 if not yet exposed via the RPS
   awx_host: "https://{{ inventory_hostname }}/"
   awx_internal_host: "http://awx.ec.local:8013/"
   awx_token: "{{ lookup('env', 'AWX_MYNEWBUBBLE_TOKEN') }}"

   # if the credential does not exist in AWX, it will be created
   awx_netbox_credential_name: netbox_token
   # the SSH credential MUST exist in AWX
   awx_ssh_credential_name: aws-default
   # if the execution environment does not exist in AWX, it will be created
   awx_execution_environment_name: "ec_awx_ee"

   # required only if the EC proxy is needed within the bubble
   proxy_enabled: false
   proxy_url: "{{ lookup('env', 'PROXY_EC_URL') }}"
   # if the credential does not exist in AWX, it will be created
   awx_proxy_credential_name: proxy_ec_url

   # required only if a job needs to access Intragate
   intragate_enabled: false
   intragate_user: "{{ lookup('env', 'INTRAGATE_API_USER') }}"
   intragate_password: "{{ lookup('env', 'INTRAGATE_API_PASSWORD') }}"
   # if the credential does not exist in AWX, it will be created
   awx_intragate_credential_name: intragate_api

``ansible/group_vars/bubble_mynewbubble/netbox.yml``:

.. code-block:: yaml
   :linenos:

   ---

   netbox_api: "{{ lookup('env', 'NETBOX_MYNEWBUBBLE_API') }}"
   netbox_internal_api: "http://netbox.ec.local:8080/"
   netbox_token: "{{ lookup('env', 'NETBOX_MYNEWBUBBLE_TOKEN') }}"

``ansible/group_vars/bubble_mynewbubble/rps.yml``:

.. code-block:: yaml
   :linenos:

   rps_internal_environments: []
   rps_external_environments: []

   rps_playbooks:
     - title: Provision Docker RPS
       local: true
       scope: mynewbubble_internal
       playbook: playbooks/provision_docker_rps/site.yml
       ## Either "lab", "prod", "preprod", or a custom RRULE
       ## see `ansible/group_vars/all/rps.yml` for more information
       #schedule: "{{ rps_service_maintenance_timewindow.lab }}"
       extra_vars:
         bubble_name: mynewbubble
         view_name: internal
         rps_waf_enabled: true
         rps_view_environments: "{{ rps_internal_environments }}"
         ## You can split this job into multiple jobs by selecting specific
         ## environments to deploy. Useful to split "acceptance" and "production"
         ## NB: They all must share the same `rps_view_environments`
         rps_deploy_environments: "{{ rps_internal_environments }}"

     - title: Provision Docker RPS
       local: true
       scope: mynewbubble_external
       playbook: playbooks/provision_docker_rps/site.yml
       ## Either "lab", "prod", "preprod", or a custom RRULE
       ## see `ansible/group_vars/all/rps.yml` for more information
       #schedule: "{{ rps_service_maintenance_timewindow.lab }}"
       extra_vars:
         bubble_name: mynewbubble
         view_name: external
         rps_waf_enabled: true
         rps_view_environments: "{{ rps_external_environments }}"
         ## You can split this job into multiple jobs by selecting specific
         ## environments to deploy. Useful to split "acceptance" and "production"
         ## NB: They all must share the same `rps_view_environments`
         rps_deploy_environments: "{{ rps_external_environments }}"

     - title: Functional tests
       local: true
       scope: mynewbubble
       playbook: playbooks/functional_tests/site.yml
       extra_vars:
         bubble_name: mynewbubble  # expects the name as it appears in the DNS zone


``ansible/group_vars/bubble_mynewbubble/certificates.yml``:

.. code-block:: yaml
   :linenos:

   certificates_playbooks: []
   certificates_netbox_webhooks: []
   certificates_ca_list:
      - letsencrypt
      - commissign

   acme_privkey_letsencrypt: |
      {{
         lookup('env', 'CERTIFICATES_ACME_PRIVKEY_LETSENCRYPT')
            | unvault(lookup('env', 'CERTIFICATES_ACME_VAULT_KEY'))
      }}
   acme_privkey_commissign: |
      {{
         lookup('env', 'CERTIFICATES_ACME_PRIVKEY_COMMISSIGN')
            | unvault(lookup('env', 'CERTIFICATES_ACME_VAULT_KEY_COMMISSIGN'))
      }}

``ansible/group_vars/bubble_mynewbubble/cmdb.yml``:

.. code-block:: yaml
   :linenos:

   ---

   # Its an FQDN (Fully Qualified Domain Name) of your netbox
   netbox_host_var: "vnetbox-130.lab.snmc.cec.eu.int" 
   # Its an name of credetials used in AWX to connect wiht Netbox host
   ssh_credential_name: "ssh_netbox"
   # AWX inventory name where netbox and vault are present. For most cases it's ``sys_service_catalogue_inventory``
   sys_inventory_name: "sys_service_catalogue_inventory"
   # Similar to ``awx_host``, two scenarios possible: ``http://{{ netbox_host_var }}:8080/api`` or ``"https://{{ netbox_host_var }}/api"``
   password_rotation_netbox_base_url: "https://{{ netbox_host_var }}/api"
   # Path where vault credentials for your Netbox are stored (see: [vault preparation](#vault-preparation))
   password_rotation_netbox_name: "workplace/netbox/{{ netbox_host_var }}/all"

For more information about the inventory, consult :doc:`this page <../architecture/index>`.

**3)** Consult :doc:`this page <new-env>` to add a new environment to the
Ansible inventory.

**4)** Go to the `CICD settings <https://code.europa.eu/digit-c4/awx-data/-/settings/ci_cd>`_
and expand the "Variables" section.

Then add the following variables:

.. csv-table::
   :header-rows: 1

   Variable,Description
   AWX_MYNEWBUBBLE_TOKEN,"Access token to the AWX API"
   NETBOX_MYNEWBUBBLE_API,"URL to the bubble's Netbox (can be it's IP address if not yet exposed via the RPS)"
   NETBOX_MYNEWBUBBLE_TOKEN,"Access token to the Netbox API"

**5)** Consult :doc:`this page <cicd>` to run the pipeline and provision the AWX.

**6)** Consult :doc:`this page <install-rps>` to install the RPS in the bubble,
if not done already.

**7)** Once the RPS is installed, update the ``ansible/hosts`` file and the
``NETBOX_MYNEWBUBBLE_API`` variable with the correct URLs.

**8)** Repeat **step 5** one last time.
