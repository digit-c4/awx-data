How To: Install the Reverse Proxy Service
=========================================

**1)** Go to the AWX "Templates" page, search for ``RPS``.

**2)** Run the ``Provision Docker RPS`` job corresponding to your target
environment and wait for it to finish (successfully).
