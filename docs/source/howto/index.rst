How To: Guides on daily operations
==================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   cicd
   new-bubble
   new-env
   install-rps
   cmdb
   use-cmdb-jobs
