How To: Run/Test CMDB Operations project from local machine 
===========================================================

To execute CMDB role you will need to set additional environment variables. They are stored in https://sam-hcpvault.cec.eu.int/ui/vault/secrets/cmdb/kv/netbox_operations%2Fpassword_rotation%2Fall/details?namespace=EC%2FDIGIT_C4_SNET_ADMIN-PROD.

Copy them, and set on VM you are using.

**1)** Environment variables needed:

.. code-block:: shell

    export PLAYBOOK="${PWD}/ansible/playbooks/provision.yml" 
    export INVENTORY="${PWD}/ansible/hosts"
    export ANSIBLE_EXTRA='-e force_sync=true'
    export MANUAL_TARGET='awx_lab_dev'
    export ANSIBLE_LOGLEVEL='-vvvv'
    export AWX_LAB_USER=snet
    export AWX_LAB_PASSWORD=<snet_ui_pass_to_use>
    export CMDB_EXTRA_NAMING=' - test_cmdb'
    export NETBOX_DB_USER='netbox'
    export NETBOX_DB_PASSWORD='netbox db password'
    export VLOG_PASSWORD=<vlog access password>
    export TAG_NAME='cmdb'
    export CMDB_PASSWD_ROT_VAULT_ROLE_ID=<Get Value from the vault>
    export CMDB_PASSWD_ROT_VAULT_SECRET_ID=<Get Value from the vault>
    export AWX_EE_REGISTRY_USERNAME=<Get Value from the vault>
    export AWX_EE_REGISTRY_URL=<Get Value from the vault>
    export AWX_EE_REGISTRY_PASSWORD=<Get Value from the vault>



***CMDB_EXTRA_NAMING variable will add to all the names some extra value to identify them.**


**2)** Run the command:

If you are using proxy, disable it before executing command below

.. code-block:: shell

    ansible-playbook $ANSIBLE_LOGLEVEL $ANSIBLE_EXTRA -i ${INVENTORY} ${PLAYBOOK} -l "${MANUAL_TARGET}" --tags "${TAG_NAME}"
