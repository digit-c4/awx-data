CMDB role architecture
=======================

This role is in charge of provisionning the AWX with the projects, inventories
and job templates needed to deploy CMDB Operations.


CMDB dump and retrieve data base backup Job
---------------------------------------------

This job is in charge of deploying the dump and retrieve data base backup on the dedicated virtual
machines.


CMDB Password rotation job
------------------------------

This job is in charge of deploying the Password rotation on the dedicated virtual
machines.
`Repository <https://code.europa.eu/digit-c4/netbox-operations/-/blob/main/docs/change_users_password/netbox_users_passwords_rotation.md?ref_type=heads/>`_.

.. image:: /_static/img/cmdb/netbox_users_passwords_rotation.png
   :alt: Schema Password rotation
