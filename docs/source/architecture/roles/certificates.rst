Certificates role architecture
==============================

This role is in charge of provisionning the AWX with the projects, inventories
and job templates needed to deploy the Certificates Proxy Service.

Certificates ACME
-----------------

This job is in charge of obtaining a certificate from an ACME CA.
For now, this job also applies the certificate to RPS.

.. image:: /_static/img/arch-certificates/schema-certificates.png
   :alt: Schema for Certificates

Certificates ACME_schedule
--------------------------

This job is in charge of checking in Netbox which certificates are going to expire, then it changes
their status to be renewed so that the ACME job above obtains a new version for each of them.
