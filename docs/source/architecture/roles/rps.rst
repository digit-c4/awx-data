RPS role architecture
=====================

This role is in charge of provisionning the AWX with the projects, inventories
and job templates needed to deploy the Reverse Proxy Service.

RPS Job architecture
-----------------------------

.. image:: /_static/img/arch-rps/schema.png
   :alt: Schema for RPS instance

For more information about the available playbooks, please refer to the
`RPS playbooks documentation <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-playbooks/>`_.
