Available roles
===============

.. toctree::
   :maxdepth: 1
   :caption: Content:

   awx
   rps
   certificates
   cmdb
