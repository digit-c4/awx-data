Project Architecture
====================

The project contains a single playbook ``ansible/playbooks/provision.yml`` used
to provision the AWX and Netbox of a bubble with the correct configuration.

Each hostgroup correspond to a specific bubble. They contain a single host,
which is the host of the bubble's AWX.

The playbook is split into multiple roles, one per service.

.. toctree::
   :maxdepth: 2
   :caption: As of today, the following roles are available:

   awx
   rps
   certificates
   cmdb

   roles/index
   configuration
   bubble-design
