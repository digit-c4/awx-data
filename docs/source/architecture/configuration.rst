Bubble configuration
====================

The configuration of a bubble is done via the Ansible inventory specific to the
hostgroup:

AWX specific inventory values
-----------------------------

.. csv-table:: AWX specific inventory values
   :file: /_data/inventory.awx.csv
   :header-rows: 1

Netbox specific inventory values
--------------------------------

.. csv-table:: Netbox specific inventory values
   :file: /_data/inventory.netbox.csv
   :header-rows: 1

RPS specific inventory values
-----------------------------

.. csv-table:: RPS specific inventory values
   :file: /_data/inventory.rps.csv
   :header-rows: 1

Certificates specific inventory values
--------------------------------------

.. csv-table:: Certificates specific inventory values
   :file: /_data/inventory.certificates.csv
   :header-rows: 1

CMDDB specific inventory values
--------------------------------------

.. csv-table:: CMDB specific inventory values
   :file: /_data/inventory.cmdb.csv
   :header-rows: 1
