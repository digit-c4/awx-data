# AWX/Netbox provisionning

This repository contains Ansible roles and playbooks used to provision the
AWX and Netbox configuration for new bubbles.

For more information:

 - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute to the repository
 - consult the [documentation](https://digit-c4.pages.code.europa.eu/awx-data/)
